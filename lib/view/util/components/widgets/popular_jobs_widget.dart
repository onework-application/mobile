import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../style/style.dart';

class PopularJobs extends StatefulWidget {
  const PopularJobs({super.key});

  @override
  State<PopularJobs> createState() => _PopularJobsState();
}

class _PopularJobsState extends State<PopularJobs> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200.w,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        itemBuilder: (contexti, index) {
          return GestureDetector(
            onTap: () {
              showBottomSheet(
                enableDrag: true,
                context: context,
                builder: (_) {
                  return Container(
                    padding: const EdgeInsets.only(top: 50),
                    decoration: const BoxDecoration(
                      color: Style.whiteColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Style.greyColor65,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                      child: Column(
                        children: [
                          20.verticalSpace,
                          Row(
                            children: [
                              20.horizontalSpace,
                              const Icon(
                                Icons.bookmark_border,
                                color: Style.greyColorCO,
                                size: 35,
                              ),
                              const Spacer(),
                              Image.asset(
                                'assets/share.png',
                                width: 32,
                                height: 32,
                              ),
                              20.horizontalSpace
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: Image.network(
                                  'https://www.pngall.com/wp-content/uploads/12/Burger-King-PNG-Photo.png',
                                  height: 50,
                                  width: 50,
                                ),
                              ),
                              16.horizontalSpace,
                              Column(
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        'text',
                                        style: Style.textStyleWidth600(),
                                      ),
                                      Text(
                                        'company',
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: Style.textStyleRegular(
                                          size: 13,
                                          textColor: Style.greyColor90,
                                        ),
                                      )
                                    ],
                                  ),
                                  20.horizontalSpace,
                                  Row(
                                    children: [
                                      30.horizontalSpace,
                                      Text(
                                        'price',
                                        softWrap: false,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineSmall,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            },
            child: Container(
                width: 300.w,
                height: 186.h,
                margin: const EdgeInsets.only(right: 24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(24),
                  color: Style.whiteColor,
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          24.horizontalSpace,
                          Container(
                            height: 46.h,
                            width: 46.w,
                            decoration: BoxDecoration(
                              image: const DecorationImage(
                                image: NetworkImage(
                                  'https://www.pngall.com/wp-content/uploads/12/Burger-King-PNG-Photo.png',
                                ),
                              ),
                              color: Style.greyColor90,
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          const Spacer(),
                          2 == 3
                              ? const Icon(
                                  Icons.bookmark_outline,
                                  size: 40,
                                  color: Style.whiteColor,
                                )
                              : const Icon(
                                  Icons.bookmark,
                                  size: 40,
                                  color: Style.primaryColor,
                                ),
                          20.horizontalSpace
                        ],
                      ),
                      24.verticalSpace,
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          'widget.companyName',
                          style: Style.textStyleWidth500(
                            size: 12,
                            textColor: const Color.fromARGB(255, 118, 118, 118),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          'widget.textPosition',
                          style: Style.textStyleWidth600(
                            size: 22,
                            textColor: Style.blackColor,
                          ),
                        ),
                      ),
                      4.verticalSpace,
                      Row(
                        children: [
                          20.horizontalSpace,
                          Text(
                            'widget.salary',
                            style: Style.textStyleWidth500(
                              size: 12,
                              textColor: Style.blackColor,
                            ),
                          ),
                          Text(
                            ' - ',
                            style: Style.textStyleRegular(
                              size: 12,
                              textColor: Style.blackColor,
                            ),
                          ),
                          Text(
                            'widget.textlocation',
                            style: Style.textStyleRegular(
                              size: 12,
                              textColor:
                                  const Color.fromARGB(255, 118, 118, 118),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )),
          );
        },
      ),
    );
  }
}
