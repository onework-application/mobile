// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../style/style.dart';

class LatestJobs extends StatelessWidget {
  const LatestJobs({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 6,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            showBottomSheet(
              enableDrag: true,
              context: context,
              builder: (_) {
                return Container(
                  padding: const EdgeInsets.only(top: 50),
                  decoration: const BoxDecoration(
                    color: Style.whiteColor,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Style.greyColor65,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: Column(
                      children: [
                        20.verticalSpace,
                        Row(
                          children: [
                            20.horizontalSpace,
                            const Icon(
                              Icons.bookmark_border,
                              color: Style.greyColorCO,
                              size: 35,
                            ),
                            const Spacer(),
                            Image.asset(
                              'assets/share.png',
                              width: 32,
                              height: 32,
                            ),
                            20.horizontalSpace
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Image.network(
                                'https://www.pngall.com/wp-content/uploads/12/Burger-King-PNG-Photo.png',
                                height: 50,
                                width: 50,
                              ),
                            ),
                            16.horizontalSpace,
                            Column(
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      'text',
                                      style: Style.textStyleWidth600(),
                                    ),
                                    Text(
                                      'company',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: Style.textStyleRegular(
                                        size: 13,
                                        textColor: Style.greyColor90,
                                      ),
                                    )
                                  ],
                                ),
                                20.horizontalSpace,
                                Row(
                                  children: [
                                    30.horizontalSpace,
                                    Text(
                                      'price',
                                      softWrap: false,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineSmall,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
          child: Container(
            margin: const EdgeInsets.only(bottom: 17),
            decoration: BoxDecoration(
              color: Style.whiteColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.network(
                    'https://www.pngall.com/wp-content/uploads/12/Burger-King-PNG-Photo.png',
                    height: 45,
                    width: 45,
                  ),
                  16.horizontalSpace,
                  Row(
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            width: 150.w,
                            child: Text(
                              'text',
                              maxLines: 1,
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              style: Style.textStyleWidth600(),
                            ),
                          ),
                          SizedBox(
                            width: 150.w,
                            child: Text(
                              'location',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: Style.textStyleRegular(
                                size: 13,
                                textColor: Style.greyColor90,
                              ),
                            ),
                          )
                        ],
                      ),
                      20.horizontalSpace,
                      Row(
                        children: [
                          30.horizontalSpace,
                          SizedBox(
                            width: 60.w,
                            child: Text(
                              'price',
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
