import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GAF extends StatelessWidget {
  const GAF({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'assets/google.png',
          height: 41.h,
          width: 41.w,
        ),
        10.horizontalSpace,
        Image.asset(
          'assets/apple.png',
          height: 41.h,
          width: 41.w,
        ),
        10.horizontalSpace,
        Image.asset(
          'assets/facebook.png',
          height: 41.h,
          width: 41.w,
        ),
      ],
    );
  }
}
