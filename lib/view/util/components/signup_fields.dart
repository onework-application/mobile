import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../../controller/app_controller.dart';
import '../style/style.dart';
import 'customtextform.dart';

class SignUpwidgets extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  final formkey;
  final TextEditingController email;
  final TextEditingController pass;
  final TextEditingController cpass;
  final TextEditingController lname;
  final TextEditingController fname;
  const SignUpwidgets(
      {super.key,
      required this.email,
      required this.pass,
      required this.cpass,
      required this.lname,
      required this.fname,
      this.formkey});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 38, left: 18),
      child: Form(
        key: formkey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'First name',
              style: Style.textStyleWidth500(
                size: 15,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            SizedBox(
              height: 50,
              child: CustomTextFrom(
                filled: true,
                fillColor: const Color(0xff535353).withOpacity(0.1),
                controller: fname,
                hintext: '',
                label: 'e.g. John',
                isObscure: false,
              ),
            ),
            10.verticalSpace,
            Text(
              'Last name',
              style: Style.textStyleWidth500(
                size: 15,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            SizedBox(
              height: 50,
              child: CustomTextFrom(
                filled: true,
                fillColor: const Color(0xff535353).withOpacity(0.1),
                controller: lname,
                label: 'e.g. Joe',
                isObscure: false,
                hintext: '',
              ),
            ),
            10.verticalSpace,
            Text(
              'Email address',
              style: Style.textStyleWidth500(
                size: 15,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            SizedBox(
              height: 50,
              child: CustomTextFrom(
                filled: true,
                fillColor: const Color(0xff535353).withOpacity(0.1),
                controller: email,
                hintext: '',
                label: 'e.g. john.doe@gmail.com',
                isObscure: false,
              ),
            ),
            10.verticalSpace,
            Text(
              'Password',
              style: Style.textStyleWidth500(
                size: 15,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            SizedBox(
              height: 50,
              child: CustomTextFrom(
                filled: true,
                fillColor: const Color(0xff535353).withOpacity(0.1),
                obscuringCharacter: '*',
                controller: pass,
                hintext: '',
                label: '********',
                isObscure: true,
              ),
            ),
            10.verticalSpace,
            Text(
              'Confirm Password',
              style: Style.textStyleWidth500(
                size: 15,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            SizedBox(
              height: 50,
              child: CustomTextFrom(
                filled: true,
                fillColor: const Color(0xff535353).withOpacity(0.1),
                controller: cpass,
                hintext: '',
                label: '********',
                isObscure: true,
              ),
            ),
            20.verticalSpace,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {
                    context.read<AppController>().whoishe(true);
                  },
                  child: Row(
                    children: [
                      Container(
                        height: 18.h,
                        width: 18.w,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Style.primaryColor,
                            width: 2,
                          ),
                        ),
                        child: context.watch<AppController>().whoIsHe
                            ? const Icon(
                                Icons.done,
                                size: 10,
                                color: Style.primaryColor,
                              )
                            : const SizedBox(),
                      ),
                      5.horizontalSpace,
                      Text(
                        'Job seeker',
                        style: Style.textStyleWidth500(
                          textColor: Style.greyColor50,
                          size: 14,
                        ),
                      ),
                    ],
                  ),
                ),
                20.horizontalSpace,
                GestureDetector(
                  onTap: () {
                    context.read<AppController>().whoishe(false);
                  },
                  child: Row(
                    children: [
                      Container(
                        height: 18.h,
                        width: 18.w,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Style.primaryColor,
                            width: 2,
                          ),
                        ),
                        child: context.watch<AppController>().whoIsHe
                            ? const SizedBox.shrink()
                            : const Icon(
                                Icons.done,
                                size: 10,
                                color: Style.primaryColor,
                              ),
                      ),
                      5.horizontalSpace,
                      Text(
                        'Employer',
                        style: Style.textStyleWidth500(
                          textColor: Style.greyColor50,
                          size: 14,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
