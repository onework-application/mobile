import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../style/style.dart';

class CustomErrorDialog extends StatelessWidget {
  final String text;
  const CustomErrorDialog({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Container(
        width: 50.w,
        height: 280.h,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(20),
          ),
          color: Style.whiteColor.withOpacity(0.8),
        ),
        child: Column(
          children: [
            20.verticalSpace,
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: Text(
                text,
                style: Style.textStyleWidth600(size: 20),
                textAlign: TextAlign.center,
              ),
            ),
            15.verticalSpace,
            const Icon(
              Icons.error,
              size: 80,
              color: Style.redColor,
            ),
            35.verticalSpace,
            Container(
              height: 1,
              width: double.infinity,
              color: Style.greyColor50,
            ),
            15.verticalSpace,
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                'OK',
                style: Style.textStyleWidth600(
                  textColor: Style.greyColor50,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
