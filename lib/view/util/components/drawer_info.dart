import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:work_one/controller/app_controller.dart';
import 'package:work_one/view/pages/general/general_page.dart';
import 'package:work_one/view/util/components/custom_button.dart';
import 'package:work_one/view/util/components/widgets/g_a_f.dart';
import '../style/style.dart';

class DrawerColumn extends StatefulWidget {
  const DrawerColumn({super.key});

  @override
  State<DrawerColumn> createState() => _DrawerColumnState();
}

class _DrawerColumnState extends State<DrawerColumn> {
  bool isChangedTheme = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: Style.whiteColor),
      child: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            10.verticalSpace,
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Image.asset(
                'assets/cancel.png',
                height: 18.h,
                width: 18.w,
              ),
            ),
            40.verticalSpace,
            // Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     Center(
            //       child: Image.asset(
            //         'assets/ava.png',
            //         height: 76.h,
            //         width: 76.w,
            //       ),
            //     ),
            //     Center(
            //       child: Text(
            //         'Sardor Ergashaliyev',
            //         style: Style.textStyleWidth600(size: 22),
            //       ),
            //     ),
            //     Center(
            //       child: Text(
            //         'Flutter Mobile',
            //         style: Style.textStyleWidth300(size: 12),
            //       ),
            //     ),
            //     20.verticalSpace,
            //     Container(
            //       height: 36.h,
            //       width: 260.w,
            //       decoration: BoxDecoration(
            //         color: Style.redColor,
            //         borderRadius: BorderRadius.circular(10),
            //       ),
            //     ),
            //     10.verticalSpace,
            //     Text(
            //       'Settings',
            //       style: Style.textStyleRegular(size: 18),
            //     ),
            //     10.verticalSpace,
            //     Text(
            //       'Settings',
            //       style: Style.textStyleRegular(size: 18),
            //     ),
            //     10.verticalSpace,
            //     Text(
            //       'Settings',
            //       style: Style.textStyleRegular(size: 18),
            //     ),
            //     10.verticalSpace,
            //     Text(
            //       'Settings',
            //       style: Style.textStyleRegular(size: 18),
            //     ),
            //   ],
            // ),
            Column(
              children: [
                Center(
                  child: Image.asset(
                    'assets/drawer_no_acc.png',
                    height: 147.h,
                    width: 175.w,
                  ),
                ),
                30.verticalSpace,
                Text(
                  'You haven’t\nOneWork account yet',
                  textAlign: TextAlign.center,
                  style: Style.textStyleWidth600(size: 22),
                ),
                30.verticalSpace,
                Padding(
                  padding: const EdgeInsets.only(left: 26, right: 46),
                  child: CustomButton(
                    onTap: () {
                      context.read<AppController>().setIndex(4);
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (_) => const GeneralPage(),
                          ),
                          (route) => false);
                    },
                    text: 'Create an account',
                    height: 46,
                    width: double.infinity,
                  ),
                ),
                10.verticalSpace,
                Text(
                  'Or continue with',
                  textAlign: TextAlign.center,
                  style: Style.textStyleWidth500(size: 12),
                ),
                10.verticalSpace,
                const GAF()
              ],
            ),
            const Spacer(),
            Text(
              'Settings',
              style: Theme.of(context).textTheme.displaySmall,
            ),
            20.verticalSpace,
            Text(
              'Help',
              style: Theme.of(context).textTheme.displaySmall,
            ),
            20.verticalSpace,
            Row(
              children: [
                Text(
                  'Log out',
                  style: Theme.of(context).textTheme.displaySmall,
                ),
                const Spacer(),
                Text(
                  'version 1.0.0',
                  style: Style.textStyleWidth300(
                    size: 14,
                  ),
                ),
                17.horizontalSpace,
              ],
            ),
            60.verticalSpace,
          ],
        ),
      ),
    );
  }
}
