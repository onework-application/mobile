import 'package:flutter/material.dart';

import '../style/style.dart';

class CustomTextFrom extends StatelessWidget {
  final String hintext;
  final Widget? suffixicon;
  final Icon? perfixicon;
  final TextEditingController? controller;
  final TextInputType keyboardType;
  final String? label;
  final ValueChanged<String>? onChange;
  final String? obscuringCharacter;
  final bool isObscure;
  final Color? fillColor;
  final bool? filled;
  final FocusNode? focusNode;
  final FormFieldValidator? validator;

  const CustomTextFrom({
    Key? key,
    required this.hintext,
    this.keyboardType = TextInputType.text,
    this.suffixicon,
    this.label,
    this.onChange,
    this.perfixicon,
    this.controller,
    this.obscuringCharacter,
    required this.isObscure,
    this.fillColor,
    this.filled,
    this.validator,
    this.focusNode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      validator: validator,
      keyboardType: keyboardType,
      obscureText: isObscure ? true : false,
      controller: controller,
      decoration: InputDecoration(
        fillColor: fillColor,
        filled: filled,
        prefix: perfixicon,
        suffix: suffixicon,
        label: Text('$label'),
        labelStyle:
            Style.textStyleRegular(textColor: Style.greyColor50, size: 15),
        hintText: hintext,
        contentPadding:
            const EdgeInsets.only(left: 24, right: 80, top: 12, bottom: 12),
        errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red, width: 0.0),
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xffAFB0B6),
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xffAFB0B6),
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Style.primaryColor),
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
      ),
    );
  }
}
