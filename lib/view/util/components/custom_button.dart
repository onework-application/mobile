import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../style/style.dart';

class CustomButton extends StatelessWidget {
  final double height;
  final double width;
  final String text;
  final VoidCallback? onTap;
  const CustomButton({
    super.key,
    required this.text,
    required this.height,
    required this.width,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height.h,
        width: width.w,
        decoration: BoxDecoration(
          color: Style.primaryColor,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Center(
          child: Text(
            text,
            style: Style.textStyleWidth500(
              textColor: Style.whiteColor,
              size: 16,
            ),
          ),
        ),
      ),
    );
  }
}
