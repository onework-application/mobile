import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class Style {
  Style._();

  // ---------- Color   ---------- //

  static const primaryColor = Color(0xff0B9D56);
  static const redColor = Colors.red;
  static const whiteColor = Colors.white;
  static const blackColor = Colors.black;
  static const darkBgcolorOfApp = Color(0xff090A11);
  static const lightBgcolorOfApp = Color(0xffF4F4F4);
  static const greyColorCO = Color(0xffC0C0C0);
  static const greyColor65 = Color(0xffEDEDED);
  static const greyColor90 = Color.fromARGB(255, 220, 220, 220);
  static const greyColor50 = Color(0xff8B8B8B);
  static const navBgcolorOfApp = Color(0xff121421);
  // ---------- Gradient   ---------- //

  // static const linearGradient = LinearGradient(
  //     begin: Alignment.bottomRight,
  //     end: Alignment.topLeft,
  //     colors: [
  //       Color(0xffFF1843),
  //       Color(0xffFF7E95),
  //     ]);

  static normal({int size = 16, Color color = whiteColor}) {
    return GoogleFonts.inter(
      fontSize: size.sp,
      color: color,
      fontWeight: FontWeight.normal,
    );
  }

  static textStyleWidth300({
    double size = 18,
    Color textColor = blackColor,
  }) =>
      GoogleFonts.poppins(
        fontWeight: FontWeight.w300,
        color: textColor,
        fontSize: size,
      );

  static textStyleRegular({
    double size = 18,
    Color textColor = blackColor,
  }) =>
      GoogleFonts.poppins(
        fontWeight: FontWeight.w400,
        color: textColor,
        fontSize: size,
      );

  static textStyleWidth500({
    double size = 18,
    Color textColor = blackColor,
  }) =>
      GoogleFonts.poppins(
        fontWeight: FontWeight.w500,
        color: textColor,
        fontSize: size,
      );

  static textStyleWidth600({
    double size = 18,
    Color textColor = blackColor,
  }) =>
      GoogleFonts.poppins(
        fontWeight: FontWeight.w600,
        color: textColor,
        fontSize: size,
      );

  static textStyleWidthBold({
    double size = 18,
    Color textColor = blackColor,
  }) =>
      GoogleFonts.poppins(
        fontWeight: FontWeight.bold,
        color: textColor,
        fontSize: size,
      );

  static textStyleWidth800({
    double size = 18,
    Color textColor = blackColor,
  }) =>
      GoogleFonts.poppins(
        fontWeight: FontWeight.w800,
        color: textColor,
        fontSize: size,
      );
}
