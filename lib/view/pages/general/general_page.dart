import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:work_one/view/pages/auth/choose_page.dart';
import 'package:work_one/view/pages/home/add_page.dart';
import 'package:work_one/view/pages/home/saved_page.dart';
import 'package:work_one/view/util/style/style.dart';
import '../../../controller/app_controller.dart';
import '../home/home_page.dart';

class GeneralPage extends StatefulWidget {
  const GeneralPage({super.key});

  @override
  State<GeneralPage> createState() => _GeneralPageState();
}

class _GeneralPageState extends State<GeneralPage> {
  List<Widget> mainPages = [
    const HomePage(),
    const SavedPage(),
    const AddPage(),
    const Placeholder(),
    const ChoosePage(),
  ];

  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    context.read<AppController>().contexti(context: context);
    return Scaffold(
      body: IndexedStack(
        index: context.watch<AppController>().currentIndex,
        children: mainPages,
      ),
      backgroundColor:
          Theme.of(context).bottomNavigationBarTheme.backgroundColor,
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Style.primaryColor,
        unselectedItemColor: Style.blackColor,
        unselectedLabelStyle: Style.textStyleWidth300(size: 12),
        selectedLabelStyle: Style.textStyleWidth300(size: 12),
        onTap: (value) {
          context.read<AppController>().setIndex(value);
        },
        currentIndex: context.watch<AppController>().currentIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home_filled,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.bookmark,
            ),
            label: 'Saved',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add_box,
            ),
            label: 'Add',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.done,
            ),
            label: 'Applied',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Feed',
          ),
        ],
      ),
    );
  }
}
