import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:work_one/controller/app_controller.dart';
import 'package:work_one/view/pages/auth/login/login_page.dart';
import 'package:work_one/view/pages/auth/register/email_page.dart';
import 'package:work_one/view/pages/general/general_page.dart';
import 'package:work_one/view/util/style/style.dart';

class ChoosePage extends StatelessWidget {
  const ChoosePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.whiteColor,
      appBar: AppBar(
        actions: [
          GestureDetector(
            onTap: () {
              context.read<AppController>().setIndex(0);
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (_) => const GeneralPage(),
                ),
                (route) => false,
              );
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Text(
                "Browse >",
                style: Style.textStyleWidth300(
                  textColor: Style.greyColor50,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100),
            child: Text(
              'Find your position with',
              textAlign: TextAlign.center,
              style: Style.textStyleWidth600(),
            ),
          ),
          15.verticalSpace,
          Container(
            height: 75.h,
            width: 75.h,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              image: DecorationImage(
                image: AssetImage('assets/logo.png'),
              ),
            ),
          ),
          20.verticalSpace,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Image.asset(
              'assets/choose.png',
              height: 300.h,
            ),
          ),
          25.verticalSpace,
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => const LogInPage(),
                ),
              );
            },
            child: Container(
              height: 52.h,
              width: 200.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Style.primaryColor,
              ),
              child: Center(
                child: Text(
                  'Log in',
                  style: Style.textStyleWidth600(textColor: Style.whiteColor),
                ),
              ),
            ),
          ),
          15.verticalSpace,
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => const EmailRegister(),
                ),
              );
            },
            child: Container(
              height: 52.h,
              width: 200.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Style.greyColor65,
              ),
              child: Center(
                child: Text(
                  'Sign up',
                  style: Style.textStyleWidth600(),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
