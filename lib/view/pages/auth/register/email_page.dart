import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:work_one/controller/app_controller.dart';
import 'package:work_one/controller/auth_controller.dart';
import 'package:work_one/view/pages/auth/register/verify_page.dart';
import 'package:work_one/view/util/components/custom_button.dart';
import 'package:work_one/view/util/components/error_dialog.dart';
import 'package:work_one/view/util/components/onunfocus.dart';
import 'package:work_one/view/util/components/signup_fields.dart';
import 'package:work_one/view/util/style/style.dart';

class EmailRegister extends StatefulWidget {
  const EmailRegister({super.key});

  @override
  State<EmailRegister> createState() => _EmailRegisterState();
}

class _EmailRegisterState extends State<EmailRegister> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final password = TextEditingController();
  final cpassword = TextEditingController();
  final fname = TextEditingController();
  final lname = TextEditingController();

  @override
  void dispose() {
    email.dispose();
    password.dispose();
    cpassword.dispose();
    fname.dispose();
    lname.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var state = context.read<AuthController>();
    return Scaffold(
      backgroundColor: Style.whiteColor,
      appBar: AppBar(
        title: Text(
          'Enter your email address',
          style: Style.textStyleWidth500(size: 22),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: Style.primaryColor,
            size: 20,
          ),
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: OnUnFocusTap(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 21),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                10.verticalSpace,
                Text(
                  'We’ll use this to sign you in or to create\nan account if you don’t have one yet',
                  style: Style.textStyleWidth500(
                    size: 12,
                    textColor: Style.greyColor50,
                  ),
                ),
                20.verticalSpace,
                SignUpwidgets(
                  email: email,
                  pass: password,
                  cpass: cpassword,
                  lname: lname,
                  fname: fname,
                ),
                50.verticalSpace,
                GestureDetector(
                  onTap: () {
                    context.read<AppController>().isSignUpFull(
                          email: email,
                          fname: fname,
                          lname: lname,
                          pass: password,
                          cpass: cpassword,
                        );
                    if (context.read<AppController>().isSigninFull == '') {
                      state.signUp(
                        email: email.text,
                        password: password.text,
                        fname: fname.text,
                        lname: lname.text,
                        type: context.read<AppController>().whoIsHe
                            ? "employee"
                            : "employer",
                        onSuccess: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => const VerifyPage(),
                            ),
                          );
                        },
                      );
                    } else {
                      showDialog(
                        context: context,
                        builder: (_) => CustomErrorDialog(
                          text:
                              '${context.read<AppController>().isSigninFull} is not correct',
                        ),
                      );
                    }
                  },
                  child: const Center(
                    child: CustomButton(
                      text: 'Submit',
                      height: 46,
                      width: 100,
                    ),
                  ),
                ),
                20.verticalSpace
              ],
            ),
          ),
        ),
      ),
    );
  }
}
