import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../../../controller/app_controller.dart';
import '../../../../controller/auth_controller.dart';
import '../../../util/components/custom_button.dart';
import '../../../util/components/customtextform.dart';
import '../../../util/components/error_dialog.dart';
import '../../../util/style/style.dart';
import '../../general/general_page.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController email = TextEditingController();

  @override
  void dispose() {
    email.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Reset password',
          style: Style.textStyleWidth600(size: 22),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: Style.primaryColor,
            size: 20,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 21),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Text(
                  'To reset your password, you need your email that can\nbe authenticated.',
                  textAlign: TextAlign.center,
                  style: Style.textStyleWidth500(
                    size: 12,
                    textColor: Style.greyColor50,
                  ),
                ),
              ),
              30.verticalSpace,
              Center(
                child: Image.asset(
                  'assets/forgot_password.png',
                  height: 330.h,
                  width: 180.w,
                ),
              ),
              70.verticalSpace,
              Padding(
                padding: const EdgeInsets.only(right: 38, left: 18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Email',
                      style: Style.textStyleWidth500(
                        size: 15,
                        textColor: Style.greyColor50,
                      ),
                    ),
                    5.verticalSpace,
                    SizedBox(
                      height: 50.h,
                      child: CustomTextFrom(
                        filled: true,
                        fillColor: const Color(0xff535353).withOpacity(0.1),
                        controller: email,
                        hintext: '',
                        isObscure: false,
                        label: 'e.g. john.doe@gmail.com',
                      ),
                    ),
                  ],
                ),
              ),
              48.verticalSpace,
              Center(
                child: CustomButton(
                  onTap: () {
                    context.read<AppController>().isForgotFull(
                          email: email,
                        );
                    if (context.read<AppController>().isLoginFull == '') {
                      context.read<AuthController>().forgotPassword(
                            email: email.text,
                            onSuccess: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => const GeneralPage(),
                                ),
                              );
                            },
                          );
                    } else {
                      showDialog(
                        context: context,
                        builder: (_) => CustomErrorDialog(
                          text:
                              '${context.read<AppController>().isLoginFull} is not correct',
                        ),
                      );
                    }
                  },
                  text: 'Send code',
                  height: 46,
                  width: 210,
                ),
              ),
              10.verticalSpace,
            ],
          ),
        ),
      ),
    );
  }
}
