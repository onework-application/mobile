import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:work_one/controller/app_controller.dart';
import 'package:work_one/controller/auth_controller.dart';
import 'package:work_one/view/pages/auth/login/forgot_paswword.dart';
import 'package:work_one/view/pages/general/general_page.dart';
import 'package:work_one/view/util/components/custom_button.dart';
import 'package:work_one/view/util/components/customtextform.dart';
import 'package:work_one/view/util/components/onunfocus.dart';

import '../../../util/components/error_dialog.dart';
import '../../../util/style/style.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({super.key});

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();

  @override
  void dispose() {
    email.dispose();
    password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OnUnFocusTap(
      child: Scaffold(
        backgroundColor: Style.whiteColor,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Wellcome Back!',
            style: Style.textStyleWidth600(size: 22),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios,
              color: Style.primaryColor,
              size: 20,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 21),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/love_man.png',
                    height: 330.h,
                    width: 180.w,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 38, left: 18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Email',
                        style: Style.textStyleWidth500(
                          size: 15,
                          textColor: Style.greyColor50,
                        ),
                      ),
                      5.verticalSpace,
                      SizedBox(
                        height: 50,
                        child: CustomTextFrom(
                          filled: true,
                          fillColor: const Color(0xff535353).withOpacity(0.1),
                          controller: email,
                          hintext: '',
                          isObscure: false,
                          label: 'e.g. john.doe@gmail.com',
                        ),
                      ),
                      20.verticalSpace,
                      Text(
                        'Password',
                        style: Style.textStyleWidth500(
                          size: 15,
                          textColor: Style.greyColor50,
                        ),
                      ),
                      5.verticalSpace,
                      SizedBox(
                        height: 50,
                        child: CustomTextFrom(
                          filled: true,
                          fillColor: const Color(0xff535353).withOpacity(0.1),
                          controller: password,
                          hintext: '',
                          isObscure: false,
                          label: '********',
                        ),
                      ),
                    ],
                  ),
                ),
                5.verticalSpace,
                Row(
                  children: [
                    const Spacer(),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => const ForgotPassword(),
                          ),
                        );
                      },
                      child: Text(
                        'Forgot password?',
                        style: Style.textStyleWidth500(
                          size: 15,
                          textColor: Style.blackColor,
                        ),
                      ),
                    ),
                    38.horizontalSpace
                  ],
                ),
                22.verticalSpace,
                Center(
                  child: CustomButton(
                    onTap: () {
                      context.read<AppController>().isLogInFull(
                            email: email,
                            pass: password,
                          );
                      if (context.read<AppController>().isLoginFull == '') {
                        context.read<AuthController>().login(
                              email: email.text,
                              password: password.text,
                              onSuccess: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => const GeneralPage(),
                                  ),
                                );
                              },
                            );
                      } else {
                        showDialog(
                          context: context,
                          builder: (_) => CustomErrorDialog(
                            text:
                                '${context.read<AppController>().isLoginFull} is not correct',
                          ),
                        );
                      }
                    },
                    text: 'Log In',
                    height: 46.h,
                    width: 210.w,
                  ),
                ),
                10.verticalSpace,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
