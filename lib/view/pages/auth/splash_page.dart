import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:work_one/view/pages/general/general_page.dart';

import '../../util/style/style.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.primaryColor,
      body: Stack(
        children: [
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            height: 250.h,
            child: Image.asset(
              fit: BoxFit.cover,
              'assets/shadow_splash.png',
            ),
          ),
          Column(
            children: [
              70.verticalSpace,
              Center(
                child: Image.asset(
                  'assets/logo.png',
                  height: 100,
                  width: 100,
                ),
              ),
              Text(
                'Find your dream job',
                textAlign: TextAlign.center,
                style: Style.textStyleWidthBold(
                  textColor: Style.whiteColor,
                  size: 50,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 250),
                child: Image.asset(
                  'assets/arrow1.png',
                  height: 150,
                  width: 150,
                ),
              ),
              const Spacer(),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 60),
                padding: const EdgeInsets.symmetric(vertical: 6),
                height: 60.h,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50.r),
                  color: Style.whiteColor,
                ),
                child: Row(
                  children: [
                    100.horizontalSpace,
                    Text(
                      'Let\'s go',
                      style: Style.textStyleWidth600(),
                    ),
                    const Spacer(),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (_) => const GeneralPage(),
                          ),
                          (route) => false,
                        );
                      },
                      child: Image.asset(
                        fit: BoxFit.cover,
                        'assets/go_button.png',
                      ),
                    ),
                    6.horizontalSpace
                  ],
                ),
              ),
              20.verticalSpace
            ],
          ),
        ],
      ),
    );
  }
}
