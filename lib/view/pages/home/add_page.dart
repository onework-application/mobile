import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:work_one/view/util/components/custom_button.dart';

import '../../util/style/style.dart';

class AddPage extends StatefulWidget {
  const AddPage({super.key});

  @override
  State<AddPage> createState() => _AddPageState();
}

class _AddPageState extends State<AddPage> {
  TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.whiteColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Add a post',
          style: Style.textStyleWidth600(size: 22),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 22),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            13.verticalSpace,
            Text(
              'Image',
              style: Style.textStyleWidth500(
                size: 18,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            const Padding(
              padding: EdgeInsets.only(left: 64, right: 86),
              child: CustomButton(
                text: 'Upload Image',
                height: 46,
                width: double.infinity,
              ),
            ),
            20.verticalSpace,
            Text(
              'Text Field',
              style: Style.textStyleWidth500(
                size: 18,
                textColor: Style.greyColor50,
              ),
            ),
            5.verticalSpace,
            Padding(
              padding: const EdgeInsets.only(right: 22),
              child: TextFormField(
                controller: controller,
                maxLines: 5,
                decoration: InputDecoration(
                  fillColor: Style.greyColor90,
                  filled: true,
                  label: const Text('Write your post text'),
                  labelStyle: Style.textStyleRegular(
                      textColor: Style.greyColor50, size: 15),
                  hintText: '',
                  contentPadding: const EdgeInsets.only(
                      left: 24, right: 80, top: 12, bottom: 12),
                  errorBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 0.0),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  border: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffAFB0B6),
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffAFB0B6),
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Style.primaryColor),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
