import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:work_one/view/util/style/style.dart';

import '../../../controller/app_controller.dart';
import '../general/general_page.dart';
import 'messege_page.dart';

class SavedPage extends StatefulWidget {
  const SavedPage({super.key});

  @override
  State<SavedPage> createState() => _SavedPageState();
}

class _SavedPageState extends State<SavedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.whiteColor,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            context.read<AppController>().setIndex(4);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (_) => const GeneralPage(),
                ),
                (route) => false);
          },
          child: const Icon(
            Icons.person_outline,
            size: 30,
            color: Style.primaryColor,
            weight: 1,
          ),
        ),
        actions: [
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => const MessegePage(),
                    ),
                  );
                },
                child: Image.asset(
                  'assets/icon_mail.png',
                  height: 30.h,
                  width: 30.w,
                ),
              ),
              18.horizontalSpace,
            ],
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 70),
            child: Center(
              child: Image.asset(
                'assets/nosaved.png',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
