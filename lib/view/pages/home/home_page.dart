import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:work_one/view/pages/home/messege_page.dart';
import 'package:work_one/view/util/components/onunfocus.dart';
import 'package:work_one/view/util/style/style.dart';

import '../../util/components/drawer_info.dart';
import '../../util/components/widgets/latest_jobs_widget.dart';
import '../../util/components/widgets/popular_jobs_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return OnUnFocusTap(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Column(
            children: [
              Text(
                'Welcome to OneWork!',
                style: Theme.of(context).textTheme.displaySmall,
              ),
              Text(
                'Discover Jobs 🔥',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ],
          ),
          elevation: 0,
          actions: [
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => const MessegePage(),
                      ),
                    );
                  },
                  child: Image.asset(
                    'assets/icon_mail.png',
                    height: 30.h,
                    width: 30.w,
                  ),
                ),
                18.horizontalSpace,
              ],
            ),
          ],
        ),
        drawer: const Drawer(
          child: SafeArea(
            child: DrawerColumn(),
          ),
        ),
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 20),
                  color: Style.whiteColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      12.verticalSpace,
                      Text(
                        'Hey, loafer',
                        style: Style.textStyleRegular(
                            textColor: Style.greyColor50),
                      ),
                      Row(
                        children: [
                          Text(
                            'Find your perfect job',
                            style: Style.textStyleWidth600(size: 22),
                          ),
                          const Spacer(),
                          Image.asset(
                            'assets/search_icon.png',
                            width: 24.w,
                            height: 24.h,
                          ),
                          24.horizontalSpace
                        ],
                      ),
                      10.verticalSpace
                    ],
                  ),
                ),
                34.verticalSpace,
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            'Popular Jobs',
                            style: Theme.of(context).textTheme.displaySmall,
                          ),
                        ],
                      ),
                      20.verticalSpace,
                      const PopularJobs(),
                      34.verticalSpace,
                      Row(
                        children: [
                          Text(
                            'Latest Jobs',
                            style: Theme.of(context).textTheme.displaySmall,
                          ),
                        ],
                      ),
                      20.verticalSpace,
                      const LatestJobs(),
                      10.verticalSpace
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
