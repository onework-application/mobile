import 'package:dio/dio.dart';

abstract class AuthFacade {
  Future<Response?> signUp({
    required String email,
    required String fname,
    required String lname,
    required String type,
    required String password,
  });

  Future<Response?> loginFacade(
      {required String email, required String password});

  Future<Response?> forgotPassword({required String email});
}
