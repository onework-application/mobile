import 'package:dio/dio.dart';
import 'package:work_one/domain/service/app_constants.dart';

class DioService {
  Dio client({String? token}) {
    return Dio(BaseOptions(
      baseUrl: 'http://kabulovdev.jprq.live/v1/',
      headers: {
        if (token != null) "Authorization": token,
      },
    ))
      ..interceptors.add(
        LogInterceptor(
          responseBody: true,
          error: true,
          requestBody: true,
          requestHeader: true,
          responseHeader: false,
        ),
      );
  }
}
