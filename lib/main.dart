import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:work_one/controller/app_controller.dart';
import 'package:work_one/controller/auth_controller.dart';
import 'package:work_one/view/pages/auth/splash_page.dart';
import 'package:work_one/view/util/style/style.dart';

Future<void> main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthController>(
          create: (_) => AuthController(),
        ),
        ChangeNotifierProvider<AppController>(
          create: (_) => AppController(),
        )
      ],
      child: ScreenUtilInit(
        designSize: const Size(390, 844),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'WorkOne',
            theme: ThemeData(
              primaryColor: Style.whiteColor,
              useMaterial3: true,
              scaffoldBackgroundColor: Style.lightBgcolorOfApp,
              textTheme: TextTheme(
                headlineLarge: Style.textStyleRegular(
                  size: 48,
                  textColor: Style.blackColor,
                ),
                titleMedium: Style.textStyleRegular(
                  size: 24,
                  textColor: const Color(0xff0D0D26),
                ),
                displayMedium: Style.textStyleRegular(
                  size: 28,
                  textColor: Style.blackColor,
                ),
                displayLarge: Style.textStyleRegular(
                  size: 34,
                  textColor: Style.blackColor,
                ),
                displaySmall: Style.textStyleRegular(
                  size: 17,
                  textColor: Style.blackColor,
                ),
                headlineSmall: Style.textStyleWidth300(
                  textColor: Style.blackColor,
                ),
              ),
            ),
            darkTheme: ThemeData(
              primaryColor: Style.darkBgcolorOfApp,
              bottomNavigationBarTheme: const BottomNavigationBarThemeData(
                backgroundColor: Style.navBgcolorOfApp,
              ),
              textTheme: TextTheme(
                headlineLarge: Style.textStyleRegular(
                  size: 48,
                  textColor: Style.whiteColor,
                ),
                titleMedium: Style.textStyleRegular(
                    size: 24, textColor: Style.whiteColor),
                displayMedium: Style.textStyleRegular(
                  size: 28,
                  textColor: Style.whiteColor,
                ),
                displayLarge: Style.textStyleRegular(
                  size: 34,
                  textColor: Style.whiteColor,
                ),
                displaySmall: Style.textStyleRegular(
                  size: 17,
                  textColor: Style.whiteColor,
                ),
                headlineSmall: Style.textStyleWidth300(
                  textColor: Style.whiteColor,
                ),
              ),
              scaffoldBackgroundColor: Style.darkBgcolorOfApp,
            ),
            home: const SplashPage(),
          );
        },
      ),
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
