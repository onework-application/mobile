import 'package:work_one/domain/service/app_validators.dart';
import 'package:work_one/domain/service/local_store.dart';
import 'package:flutter/material.dart';

class AppController extends ChangeNotifier {
  String isSigninFull = '.';
  String isLoginFull = '.';
  String isforgotFull = '.';
  bool whoIsHe = false;
  bool isChangeTheme = false;
  int currentIndex = 0;
  BuildContext? context1;

  contexti({required BuildContext context}) {
    context1 = context;
  }

  whoishe(bool bul) {
    whoIsHe = bul;
    notifyListeners();
  }

  unfocus({
    required FocusNode currentFocus,
    FocusNode? currentFocus1,
    FocusNode? currentFocus2,
  }) {
    currentFocus.unfocus();
    currentFocus1?.unfocus();
    currentFocus2?.unfocus();
  }

  getTheme() async {
    isChangeTheme = await LocalStore.getTheme();
    notifyListeners();
  }

  change() {
    isChangeTheme = !isChangeTheme;
    notifyListeners();
  }

  setIndex(int index) {
    currentIndex = index;
    notifyListeners();
  }

  isSignUpFull({
    required TextEditingController email,
    required TextEditingController fname,
    required TextEditingController lname,
    required TextEditingController pass,
    required TextEditingController cpass,
  }) {
    if (!AppValidators.isValidEmail(email.text)) {
      isSigninFull = "Email";
    } else if (!AppValidators.isValidName(fname.text)) {
      isSigninFull = "First name";
    } else if (!AppValidators.isValidName(lname.text)) {
      isSigninFull = "Last name";
    } else if (!AppValidators.isValidPassword(pass.text)) {
      isSigninFull = "Password";
    } else if (!AppValidators.isValidConfirmPassword(cpass.text, pass.text)) {
      isSigninFull = "Confirm Password";
    } else {
      isSigninFull = '';
    }
  }

  isLogInFull({
    required TextEditingController email,
    required TextEditingController pass,
  }) {
    if (!AppValidators.isValidEmail(email.text)) {
      isLoginFull = "Email";
    } else if (!AppValidators.isValidPassword(pass.text)) {
      isLoginFull = "Password";
    } else {
      isLoginFull = '';
    }
  }

  isForgotFull({
    required TextEditingController email,
  }) {
    if (!AppValidators.isValidEmail(email.text)) {
      isforgotFull = "Email";
    } else {
      isforgotFull = '';
    }
  }
}
