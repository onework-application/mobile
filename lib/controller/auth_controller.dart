import 'package:flutter/material.dart';

import '../domain/interface/auth_facade.dart';
import '../domain/repo/auth_repo.dart';

class AuthController extends ChangeNotifier {
  final AuthFacade authRepo = AuthRepo();
  bool isSignUpLoading = false;
  String currentEmail = '';

  signUp({
    required String email,
    required String password,
    required String fname,
    required String lname,
    required String type,
    required VoidCallback onSuccess,
  }) async {
    isSignUpLoading = true;
    notifyListeners();

    var res = await authRepo.signUp(
      email: email,
      password: password,
      fname: fname,
      lname: lname,
      type: type,
    );
    currentEmail = email;
    if (res?.statusCode == 200) {
      isSignUpLoading = false;
      notifyListeners();
      onSuccess();
    } else {
      print('Auth signUp res messege ${res?.statusMessage}');
    }
  }

  login({
    required String email,
    required String password,
    required VoidCallback onSuccess,
  }) async {
    isSignUpLoading = true;
    notifyListeners();

    var res = await authRepo.loginFacade(
      email: email,
      password: password,
    );
    currentEmail = email;
    if (res?.statusCode == 200) {
      isSignUpLoading = false;
      notifyListeners();
      onSuccess();
    } else {
      print('Auth login res messege ${res?.statusMessage}');
    }
  }

  forgotPassword({
    required String email,
    required VoidCallback onSuccess,
  }) async {
    isSignUpLoading = true;
    notifyListeners();

    var res = await authRepo.forgotPassword(
      email: email,
    );
    currentEmail = email;
    if (res?.statusCode == 200) {
      isSignUpLoading = false;
      notifyListeners();
      onSuccess();
    } else {
      print('Auth forgotpass res messege ${res?.statusMessage}');
    }
  }
}
